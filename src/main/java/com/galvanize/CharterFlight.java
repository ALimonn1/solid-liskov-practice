package com.galvanize;

public class CharterFlight extends Flight {

    private static final int AVERAGE_FOOD_WEIGHT = 50;
    private boolean catered;

    public CharterFlight(String departingAirport, String arrivingAirport, int capacity, int weightLimit) {
        super(departingAirport, arrivingAirport, capacity, weightLimit);
    }

    @Override
    protected int totalWeight() {
        int additionalWeight = isCatered() ? Passenger.AgeGroup.ADULT.getAverageWeight() + AVERAGE_FOOD_WEIGHT : 0;
        return super.totalWeight() + additionalWeight;
    }

    public boolean isCatered() {
        return catered;
    }

    public void setCatered(boolean catered) {
        this.catered = catered;
    }
}
